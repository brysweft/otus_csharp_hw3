﻿using System;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace otus_csharp_hw3_webclient
{
    static class Program
    {

        static readonly HttpClient client = new() { BaseAddress = new Uri("https://localhost:7277") };

        static async Task Main(string[] args)
        {

            Console.WriteLine("***** Random client *****");

            while (true)
            {
                await WriteConcoleMenu();

                string answer = Console.ReadLine();
                if (answer == "1")
                {
                    long Id = await CreareCustomer(client);
                    if(Id != 0)
                    {
                        Console.WriteLine($"Создан новый пользователь с Id: {Id}");
                    }
                    
                }
                else if (answer == "2")
                {
                    Console.Write("Введите Id: ");
                    string? StrId = Console.ReadLine();
                    if (long.TryParse(StrId, out long Id))
                    {
                        Customer customer =  await GetCustomerById(Id, client);
                        if (customer != null)
                        {
                            Console.WriteLine("Найденный пользователь: {0}", customer);
                        }  
                    }
                    else {
                        Console.WriteLine("Введен некорректный Id.");
                    }
                }
                else if (answer == "3")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Неизвестная команда");
                }

            }

        }

        static async Task WriteConcoleMenu()
        {
            Console.WriteLine("Меню:");
            Console.WriteLine("Создать пользователя: [1]");
            Console.WriteLine("Найти пользователя по Id: [2]");
            Console.WriteLine("Выйти: [3]");
            Console.Write("Выберите действие: ");
        }

        public static async Task<long> CreareCustomer(HttpClient client)
        {
            string url = $"/api/v1.0/customers/";

            CustomerCreateRequest customer = await RandomCustomer();

            var json = JsonSerializer.Serialize(customer);
            var data = new System.Net.Http.StringContent(json, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(url, data);
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                return Int64.Parse(result);
            }
            else {
                Console.WriteLine("Статусный код ошибки: {0} - {1}",
               (int)response.StatusCode, response.StatusCode);
                return 0;
            }
        }

        private static async Task<CustomerCreateRequest> RandomCustomer()
        {
            CustomerCreateRequest ccr = new CustomerCreateRequest();
            Console.WriteLine(ccr.ToString());
            return ccr;
        }

        public static async Task<Customer> GetCustomerById(long Id, HttpClient client) {

            string url = $"/api/v1.0/customers/{Id}";

            using var response = await client.GetAsync(url);

            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonSerializer.Deserialize<Customer>(result);
            }
            else
            {
                Console.WriteLine("Статусный код ошибки: {0} - {1}",
                            (int)response.StatusCode, response.StatusCode);
                return null;
            }
        }

    }

}