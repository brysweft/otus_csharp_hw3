﻿
using System.Text.Json.Serialization;

namespace otus_csharp_hw3_webclient
{
    public class Customer
    {
        [JsonPropertyName("id")]
        public long Id { get; init; }

        [JsonPropertyName("firstName")]
        public string FirstName { get; init; }

        [JsonPropertyName("lastName")]
        public string LastName { get; init; }

        public override string ToString()
        {
            return $"Id:{Id}: FirstName: {FirstName} LastName: {LastName}";
        }

    }

}
