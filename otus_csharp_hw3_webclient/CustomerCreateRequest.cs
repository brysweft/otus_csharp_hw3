﻿
namespace otus_csharp_hw3_webclient
{
    public class CustomerCreateRequest
    {
        public CustomerCreateRequest()
        {
            Random rdm = new Random();

            FirstName = ((PopularFirstNames)rdm.Next(10)).ToString();
            LastName = ((PopularLastNames)rdm.Next(10)).ToString();
        }

        public CustomerCreateRequest(
            string firstName,
            string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public string FirstName { get; init; }

        public string LastName { get; init; }

        public override string ToString()
        {
            return $"FirstName: {FirstName} LastName: {LastName}";
        }
    }

    enum PopularFirstNames { 
        Alex,
        Anna,
        Vasiliy,
        Maria,
        John,
        Liza,
        Nick,
        Kristian,
        Jack,
        Vicky
    }

    enum PopularLastNames
    {
        Jackson,
        Lee,
        Applensky,
        Gorbatko,
        Carlson,
        Greatham,
        Mikale,
        Belik,
        Frendlich,
        Bacham
    }

}
