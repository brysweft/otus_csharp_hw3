﻿using otus_csharp_hw3_webapi.Models;

namespace otus_csharp_hw3_webapi.Data
{
    public static class DbInitializer
    {
        public static void Initialize(ApplicationDbContext context)
        {

            if (context.Customers.Any())
            {
                return;
            }

            var customers = new Customer[]
            {
                new Customer{FirstName="Carson", LastName="Alexander"},
                new Customer{FirstName="Meredith", LastName="Alonso"},
                new Customer{FirstName="Arturo", LastName="Anand"},
                new Customer{FirstName="Gytis", LastName="Barzdukas"},
                new Customer{FirstName="Yan", LastName="Li"},
                new Customer{FirstName="Peggy", LastName="Justice"},
                new Customer{FirstName="Laura",LastName="Norman"},
                new Customer{FirstName="Nino",LastName="Olivetto"}
            };

            context.Customers.AddRange(customers);
            context.SaveChanges();

        }

    }
}
