﻿namespace otus_csharp_hw3_webapi.Data
{
    public static class СonnectionStringBuilder
    {
        public static string? connectionString;

        public static string Build() {

            if (connectionString == null)
            {
                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.SetBasePath(Directory.GetCurrentDirectory());
                builder.AddJsonFile("appsettings.json");
                IConfigurationRoot config = builder.Build();

                connectionString = config.GetConnectionString("DefaultConnection");

                return connectionString;

            }
            else
            {
                return connectionString;
            }
            
        
        }
    }
}
