﻿using System.ComponentModel.DataAnnotations;

namespace otus_csharp_hw3_webapi.Models
{
    public class Customer
    {
        public long Id { get; init; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

    }

}
