﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using otus_csharp_hw3_webapi.Data;
using otus_csharp_hw3_webapi.Models;

namespace otus_csharp_hw3_webapi.Controllers
{
    [Route("api/v1.0/customers")]
    public class CustomerController : ControllerBase
    {

        private readonly Data.ApplicationDbContext _context;
        private readonly ILogger<CustomerController> _logger;

        public CustomerController(ApplicationDbContext context, ILogger<CustomerController> logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpGet("{id:long}")]
        public ActionResult<Customer> GetCustomerAsync([FromRoute] long id)
        {
            var customer = _context.Customers.SingleOrDefault(p => p.Id == id);
            if (customer != null)
            {
                return Ok(customer);
            }

            return NotFound();
        }

        [HttpPost]
        [ActionName(nameof(CreateCustomerAsync))]
        public async Task<ActionResult<long>> CreateCustomerAsync([FromBody] Customer customer)
        {
            var currentCustomer = _context.Customers.SingleOrDefault(p => p.Id == customer.Id);
            if (currentCustomer != null)
            {
                return Conflict("Пользователь с таким Id уже существует в базе!");
            }

            this._context.Customers.Add(customer);
            await this._context.SaveChangesAsync();

            return Ok(customer.Id);
        }

        [HttpPut()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> EditCustomerAsync([FromBody] Customer customer)
        {
            var currentCustomer = await _context.Customers.FindAsync(customer.Id);
            if (currentCustomer == null)
            {
                return NotFound();
            }

            currentCustomer.FirstName = customer.FirstName;
            currentCustomer.LastName  = customer.LastName;

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpDelete("{id:long}")]
        public async Task<IActionResult> DeleteCustomerAsync(long id)
        {
            var customer = await this._context.Customers.FindAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            _context.Remove(customer);
            await _context.SaveChangesAsync();

            return Ok();
        }

    }

}
